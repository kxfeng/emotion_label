EMOTION_ANGER = "anger"
EMOTION_SADNESS = "sadness"
EMOTION_CONTEMPT = "contempt"
EMOTION_DISGUST = "disgust"
EMOTION_FEAR = "fear"
EMOTION_HAPPINESS = "happiness"
EMOTION_NEUTRAL = "neutral"
EMOTION_SURPRISE = "surprise"

EMOTION_ZERO = "zero"
EMOTION_ERROR = "error"

