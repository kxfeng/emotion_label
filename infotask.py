import os
import re
import sys
import zipfile

import texttable


def usage():
    print("usage:\ninfotask.py <zip_path>")


def get_zip_file_info(zip_path):
    """
    Get file info of a zip file
    :param zip_path: zip file path
    :return: file info of a zip file, for example:
    {".": ["xxx.mp4", "yyy.png" , "aaa.xml", "..."],"anger":[],"sadness":[]}
    """
    zip_file = zipfile.ZipFile(zip_path, 'r')
    list_files = zip_file.namelist()

    file_info = {}

    for f in list_files:
        # some zip may contain folder in namelist
        if f.endswith("/"):
            continue
        sep_count = f.count("/")
        if sep_count >= 2:
            continue

        if sep_count == 0:
            # root dir
            type_group = "."
            file_name = f
        else:
            # sub dir
            part = f.split("/")
            type_group = part[0]
            file_name = part[1]

        if file_info.get(type_group, None) is None:
            file_info[type_group] = []
        file_info[type_group].append(file_name)

    for key, value in file_info.items():
        value.sort()
    return file_info


def calc_pic_count_info(file_info):
    """
    Calculate pic count of each emotion
    :param file_info:  file info of a zip file
    :return: pic count info , for example:
    {".":[{"vid":"xxx.mp4","pic": 16,"xml": 16}], "anger": [], "sadness":[]}
    """
    pic_info = {}

    for key, value in file_info.items():
        pic_info[key] = []

        item = None
        for f in value:
            if re.search("(\.mp4|\.mkv)$", f, re.IGNORECASE):
                item = {"vid": f, "pic": 0, "xml": 0}
                pic_info[key].append(item)
            if re.search("(\.jpg|\.png)$", f, re.IGNORECASE):
                item["pic"] = item["pic"] + 1
            if re.search("\.xml$", f, re.IGNORECASE):
                item["xml"] = item["xml"] + 1
    return pic_info


def write_pic_count_info(to_path, pic_count_info):
    text_file = open(to_path, "w")

    for idx_key, (key, value) in enumerate(sorted(pic_count_info.items())):
        if key != ".":
            text_file.write("%s\n" % key)
        tab = texttable.Texttable()
        tab.add_row(["index", "video", "pic_count"])
        for idx, item in enumerate(value):
            if item["pic"] == item["xml"]:
                tab.add_row([idx + 1, item["vid"], item["pic"]])
            else:
                tab.add_row([idx + 1, item["vid"], "ERROR:pic=%s xml=%s" % (item["pic"], item["xml"])])

        text_file.write(tab.draw())

        if idx_key != len(pic_count_info.items()) - 1:
            text_file.write("\n\n")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        usage()
        sys.exit()

    if not os.path.isfile(sys.argv[1]):
        print("file not exist")
        sys.exit()

    zip_path = sys.argv[1]
    zip_info = get_zip_file_info(zip_path)
    count_info = calc_pic_count_info(zip_info)
    write_pic_count_info("%s.txt" % zip_path, count_info)
    print("success")
