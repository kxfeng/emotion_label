#!/usr/bin/python3
# coding: utf-8

import cv2
import numpy
from PIL import Image
from PIL import ImageDraw


class Detector:
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

    # 返回face(x, y, width, height)
    def detect_faces(self, numpy_arr, only_face=None):
        if numpy_arr.ndim == 3:
            gray = cv2.cvtColor(numpy_arr, cv2.COLOR_BGR2GRAY)
        else:
            gray = numpy_arr

        result = []
        faces = self.face_cascade.detectMultiScale(gray, 1.2, 5)

        for (x, y, width, height) in faces:
            result.append((x, y, width, height))

        if len(result) > 0 and only_face:
            return result[:1]
        return result

    def draw_faces(self, numpy_arr):
        faces = self.detect_faces(numpy_arr)

        if faces:
            image = Image.fromarray(numpy_arr)
            draw_instance = ImageDraw.Draw(image)
            for (x1, y1, x2, y2) in faces:
                draw_instance.rectangle((x1, y1, x2, y2), outline=(255, 0, 0))

            return len(faces) > 0, numpy.array(image)
        return False, numpy_arr
