import sys
import time

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import config
import const

api_url = 'https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize'
api_key = '14a76824688d4cb09d569e0885d2b703'
http_max_retries = 3

API_KEY_LIST = [
    # '14a76824688d4cb09d569e0885d2b703',
    # 'ff33c95b6eee4527925084eacb815c90',  # emotion_f0_free
    # 'c43dc8fb6e7d45369e9d204e2fb8e587',  # emotion_f0
    'eb195bc955ac46979846027b8d68ff4b'  # emotion_s0
]

# remove proxy warning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def get_api_key():
    index = config.get_key_index()
    if index >= len(API_KEY_LIST):
        print("API key out of usage")
        exit()
    return API_KEY_LIST[index]


def switch_next_key(headers):
    config.set_key_index(config.get_key_index() + 1)
    headers['Ocp-Apim-Subscription-Key'] = get_api_key()


def process_request(json, data, headers, params):
    """
    Helper function to process the request to Project Oxford

    Parameters:
    json: Used when processing images from its URL. See API Documentation
    data: Used when processing image read from disk. See API Documentation
    headers: Used to pass the key information and the data type request
    """

    retries = 0
    result = None

    while True:
        response = requests.request('post', api_url, json=json, data=data, headers=headers, params=params, verify=False)

        if response.status_code == 401:
            print('Error: invalid api key, switch to next')
            switch_next_key(headers)
            time.sleep(2)
            continue
        elif response.status_code == 403:
            print('Error: Out of key call volume, switch to next')
            switch_next_key(headers)
            time.sleep(2)
            continue
        elif response.status_code == 429:
            print("Error: Rate limit is exceeded, sleep 5s")
            if retries <= http_max_retries:
                time.sleep(5)
                retries += 1
                continue
            else:
                print('Error: Rate limit failed after retrying!')
                break

        elif response.status_code == 200 or response.status_code == 201:

            if 'content-length' in response.headers and int(response.headers['content-length']) == 0:
                result = None
            elif 'content-type' in response.headers and isinstance(response.headers['content-type'], str):
                if 'application/json' in response.headers['content-type'].lower():
                    result = response.json() if response.content else None
        else:
            print("Error code: %d" % (response.status_code))

        break

    return result


def detect_file(file_path):
    with open(file_path, 'rb') as f:
        data = f.read()

    headers = dict()
    headers['Ocp-Apim-Subscription-Key'] = get_api_key()
    headers['Content-Type'] = 'application/octet-stream'

    json = None
    params = None

    return process_request(json, data, headers, params)


def find_final_emotion(result_json):
    if result_json is None:
        return {"type": const.EMOTION_ERROR}
    if len(result_json) == 0:
        return {"type": const.EMOTION_ZERO}

    max_key = None
    max_value = 0
    max_index = 0
    for index, face_item in enumerate(result_json):
        scores = face_item["scores"]

        c_max_key = None
        c_max_value = 0
        for k, v in scores.items():
            if v > c_max_value:
                c_max_key = k
                c_max_value = v

        if c_max_value > max_value:
            max_key = c_max_key
            max_value = c_max_value
            max_index = index

    result = {}
    result["type"] = max_key
    result["value"] = max_value
    max_face_rect = result_json[max_index]["faceRectangle"]
    result["left"] = max_face_rect["left"]
    result["top"] = max_face_rect["top"]
    result["width"] = max_face_rect["width"]
    result["height"] = max_face_rect["height"]
    return result


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Image file path is needed")
        sys.exit()

    result = detect_file(sys.argv[1])
    print(result)
    emotion_result = find_final_emotion(result)
    print("Emotion result: %s" % emotion_result)
