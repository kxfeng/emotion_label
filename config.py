import configparser
import sys
from os import path

import util

config = configparser.ConfigParser()


def init():
    config.read(get_config_file())

    update = False
    if not config.has_section("KEY_INFO"):
        config.add_section("KEY_INFO")
        config.set("KEY_INFO", "index", "0")
        update = True

    if not config.has_option("KEY_INFO", "index"):
        config.set("KEY_INFO", "index", "0")
        update = True

    if not util.is_int(config.get("KEY_INFO", "index")):
        config.set("KEY_INFO", "index", "0")
        update = True

    if update:
        write_config_file()


def write_config_file():
    with open(get_config_file(), 'w') as configfile:
        config.write(configfile)


def get_config_file():
    return path.join(path.dirname(path.abspath(__file__)), "config.ini")


def get_key_index():
    return config.getint("KEY_INFO", "index")


def set_key_index(index):
    if not isinstance(index, int):
        print("Index should be int")
        return
    config.set("KEY_INFO", "index", str(index))
    write_config_file()


# init config
init()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(type(get_key_index()), " ", get_key_index())
    else:
        set_key_index(get_key_index() + 1)
        print(type(get_key_index()), " ", get_key_index())
