from unittest import TestCase

from util import pure_public_file_name


class TestUtil(TestCase):
    def test_fake_file_name(self):
        self.assertEqual("When.A.Snail.Falls.In.Love.E21_041750.jpg",
                         pure_public_file_name("When.A.Snail.Falls.In.Love.E21.WEB-DL.1080p.H264.AAC-HDCTV_041750.jpg"))

        self.assertEqual("Medical.Examiner.Dr.Qin.2016.EP10_004320.jpg",
                         pure_public_file_name("Medical.Examiner.Dr.Qin.2016.EP10.WEB-DL.1080p.H264.AAC2.0-PureTV_004320.jpg"))

        self.assertEqual("[WuJianDaoⅠ].Infernal.Affairs.Ⅰ.2002.jpg",
                         pure_public_file_name("[无间道Ⅰ(国粤双语)].Infernal.Affairs.Ⅰ.2002.Bluray.720P.x264.AC3.2Audio-CMCT.jpg"))
