#!/usr/bin/python
# -*- coding: utf-8 -*-

import concurrent.futures
import getopt
import json
import math
import os
import re
import shutil
import sys
import time
import zipfile
from os import path

import cv2
from PIL import Image
from moviepy.editor import *
from tqdm import tqdm

import const
import emotionapi
import labelxml
import logger
import sqlitehelper
import util
from sqlitehelper import SQLiteHelper

sqlite_helper = None
progress_bar = None
mlogger = None


def usage():
    print("usage:")
    print("-h   : print this help message and exit (also --help)")
    print("--cp_all_emo [--db] [--out_prefix] --type sadness --gt 0.7 dest_dir")
    print("--cp_pic_list [--db] [--out_prefix] index.txt dest_dir")
    print("--cp_vid_list [--db] [--out_prefix] index.txt dest_dir [output.json]")
    print("--det_vid_list output.json dest_dir")
    print("--label_pic_list pic_dir dest_dir")
    print("--test arg")


def zip_dir(to_file, src_dir):
    zip_f = zipfile.ZipFile(to_file, 'w', zipfile.ZIP_DEFLATED)

    for root, dirs, files in os.walk(src_dir):
        for file in files:
            file_path = os.path.join(root, file)
            zip_f.write(file_path, os.path.relpath(file_path, src_dir))
    zip_f.close()


def read_file_lines(file_name):
    lines = []
    with open(file_name) as f:
        for line in f:
            strip_line = line.strip(' \t\n\r')
            if strip_line:
                lines.append(strip_line)
    return lines


def copy_pic_xml(list_rows, dest_dir):
    """
    拷贝图片并生成xml
    """
    util.create_if_not_exist(dest_dir)
    for index, row in enumerate(list_rows):
        src_file_pic = util.path_join(row["thumb_dir"], row["thumb_p_name"])

        dest_pic_path = util.path_join(dest_dir, row["thumb_p_name"])
        util.copy_to_file(src_file_pic, dest_pic_path)

        labelxml.create_label_file(
            dest_pic_path, row[sqlitehelper.VIDEO_WIDTH], row[sqlitehelper.VIDEO_HEIGHT],
            row[sqlitehelper.EMOTION_TYPE],
            row[sqlitehelper.FACE_LEFT], row[sqlitehelper.FACE_TOP], row[sqlitehelper.FACE_WIDTH],
            row[sqlitehelper.FACE_HEIGHT])

        util.flush_print("*")
    print("")


def cmd_test(to_file, src_dir):
    zip_dir(to_file, src_dir)


def cmd_copy_all_emotion(dest_dir, emotion_type=None, gt_value=0.7, out_prefix="out"):
    """
    打包符合条件的表情
    """
    util.create_if_not_exist(dest_dir)

    if emotion_type is None:
        emotion_list = ['sadness', 'anger']
    else:
        emotion_list = [emotion_type]

    dest_temp_dirs = []
    for emotion in emotion_list:
        cursor = sqlite_helper.query_emotion([emotion], gt=gt_value, min_size=200)
        list_rows = cursor.fetchall()

        print("Emotion:%s count %d to copy." % (emotion, len(list_rows)))
        temp_dir = util.path_join(dest_dir, emotion)
        dest_temp_dirs.append(temp_dir)
        copy_pic_xml(list_rows, temp_dir)

        zip_file = util.path_join(dest_dir, '%s_emo_list_%s_%s.zip' % (out_prefix, emotion, util.get_mmddhhmm()))
        zip_dir(zip_file, util.path_join(dest_dir, emotion))

    instr = input("Delete temp output dir? y/n:")
    if instr in ("y", 'Y', 'yes', 'YES'):
        for temp_dir in dest_temp_dirs:
            shutil.rmtree(temp_dir)


def cmd_copy_pic_list(index_file, dest_dir, out_prefix="out"):
    """
    根据图片列表打包图片（图片转换使用）
    """
    dest_temp_dir = util.path_join(dest_dir, "output")
    util.create_if_not_exist(dest_temp_dir)

    lines = read_file_lines(index_file)
    for line in lines:
        sql = "SELECT * FROM %s WHERE %s = '%s'" % \
              (sqlitehelper.TABLE_NAME, sqlitehelper.THUMB_P_NAME, line)
        list_row = sqlite_helper.query(sql).fetchall()

        if len(list_row) == 0:
            print("not found in databse :%s" % line)
            continue

        row = list_row[0]
        pic_path = util.path_join(row[sqlitehelper.THUMB_DIR], row[sqlitehelper.THUMB_P_NAME])

        if not path.isfile(pic_path):
            print("not found pic file:%s" % line)
            continue

        new_pic_file_path = util.path_join(dest_temp_dir, row[sqlitehelper.THUMB_P_NAME])
        util.copy_to_file(pic_path, new_pic_file_path)
        labelxml.create_label_file(
            new_pic_file_path, row[sqlitehelper.VIDEO_WIDTH], row[sqlitehelper.VIDEO_HEIGHT],
            row[sqlitehelper.EMOTION_TYPE],
            row[sqlitehelper.FACE_LEFT], row[sqlitehelper.FACE_TOP], row[sqlitehelper.FACE_WIDTH],
            row[sqlitehelper.FACE_HEIGHT])
        util.flush_print('.')
    zip_dir(util.path_join(dest_dir, "%s_pic_list_%s.zip" % (out_prefix, util.get_mmddhhmm())), dest_temp_dir)

    instr = input("Delete temp output dir? y/n:")
    if instr in ("y", 'Y', 'yes', 'YES'):
        shutil.rmtree(dest_temp_dir)


def cmd_copy_vid_list(index_file, dest_dir, json_file=None, out_prefix="out"):
    """
    根据文件列表截取10s视频列表
    """
    dest_temp_dir = util.path_join(dest_dir, "output")
    util.create_if_not_exist(dest_temp_dir)
    lines = read_file_lines(index_file)
    lines = list(set(lines))
    lines.sort()

    if json_file is None:
        list_video_info = []
        json_file = util.path_join(dest_dir, "%s_vid_base_list_%s.json" % (out_prefix, util.get_mmddhhmm()))
    else:
        with open(util.path_join(dest_dir, json_file)) as file:
            list_video_info = json.load(file)

    for index, line in enumerate(lines):
        if index < len(list_video_info):
            print("video already created:%s" % line)
            continue

        sql = "SELECT * FROM %s WHERE %s = '%s'" % \
              (sqlitehelper.TABLE_NAME, sqlitehelper.THUMB_P_NAME, line)
        list_row = sqlite_helper.query(sql).fetchall()

        if len(list_row) == 0:
            print("not found in databse :%s" % line)
            continue

        row = list_row[0]
        video_path = util.path_join(row[sqlitehelper.VIDEO_DIR], row[sqlitehelper.VIDEO_NAME])
        if not path.isfile(video_path):
            print("not found video file:%s" % line)
            continue

        print("start curt file:%s/%s" % (index + 1, len(lines)))

        base_video_frame_name = util.file_name_no_ext(row[sqlitehelper.THUMB_P_NAME]) + "_base.mp4"
        base_video_frame_path = util.path_join(dest_temp_dir, base_video_frame_name)

        video = VideoFileClip(video_path)
        fps = video.fps
        frame_second = row[sqlitehelper.FRAME_INDEX] / fps
        video = video.subclip(frame_second - 5, frame_second + 5)

        video.write_videofile(base_video_frame_path, threads=4, verbose=False, progress_bar=False)

        video_info = {
            "base_file": base_video_frame_path,
            "start_frame": 0,
            "end_frame": 0,
            "complete_frame": 0,
            "complete": False,
            "emotion": row[sqlitehelper.EMOTION_TYPE]
        }
        list_video_info.append(video_info)

        with open(util.path_join(dest_dir, json_file), "w", encoding='utf-8') as file:
            json.dump(list_video_info, file, ensure_ascii=False, indent=4)

    zip_dir(util.path_join(dest_dir, util.file_name_no_ext(json_file) + ".zip"), dest_temp_dir)
    instr = input("Delete temp output dir? y/n:")
    if instr in ("y", 'Y', 'yes', 'YES'):
        shutil.rmtree(dest_temp_dir)


def cmd_detect_vid_list(json_file, dest_dir):
    """
    批量检测json文件列表中视频表情
    """

    def final_vid_name_creator(base_file, start_frame):
        if util.file_name_no_ext(base_file).endswith("_base"):
            """根据图片筛选切割出的视频，文件名已包含帧序号"""
            return re.sub("_base", "", util.file_name_no_ext(base_file))
        else:
            """手工指定的完整视频文件"""
            return "%s_%s" % (util.file_name_no_ext(base_file), start_frame)

    final_zip_name = re.sub("_base", "_final", util.file_name_no_ext(json_file) + ".zip")

    common_detect_vid_list(json_file, dest_dir, final_vid_name_creator, final_zip_name)


def common_detect_vid_list(json_file, dest_dir, final_vid_name_creator, final_zip_name):
    global progress_bar

    with open(util.path_join(dest_dir, json_file)) as file:
        list_video_info = json.load(file)

    dest_temp_dir = util.path_join(dest_dir, "output")
    util.create_if_not_exist(dest_temp_dir)

    list_video_name = []
    progress_bar = tqdm(total=(len(list_video_info)), bar_format='{desc}{percentage:3.0f}%|{bar}|{elapsed}')
    try:
        for index, video_info in enumerate(list_video_info):
            final_vid_name = final_vid_name_creator(video_info["base_file"], video_info['start_frame'])
            list_video_name.append(final_vid_name)
            if not video_info["complete"]:
                detect_video_process(util.path_join(dest_dir, json_file),
                                     list_video_info, index, dest_temp_dir, final_vid_name)
            progress_bar.update(1)
    except KeyboardInterrupt:
        print("interrupt end")
        return
    print("start zip video files")

    def zip_output_filter(file_prefix):
        return file_prefix in list_video_name

    detect_zip_files(final_zip_name, dest_dir, dest_temp_dir, zip_output_filter)
    progress_bar.close()

    instr = input("Delete temp output dir? y/n:")
    if instr in ("y", 'Y', 'yes', 'YES'):
        shutil.rmtree(dest_temp_dir)


def detect_zip_files(final_zip_name, dest_dir, dest_temp_dir, zip_output_filter):
    """
    打包视频结果
    """
    zip_file_path = util.path_join(dest_dir, final_zip_name)

    zipf = zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED)

    for root, dirs, files in os.walk(dest_temp_dir):
        for file in files:
            if re.search(r'\d+\.mp4$', file) is not None:
                if zip_output_filter(util.file_name_no_ext(file)):
                    video_file_path = os.path.join(root, file)
                    zipf.write(video_file_path, os.path.relpath(video_file_path, dest_temp_dir))

            if re.search(r'\d+\.xml$', file) is not None:
                if zip_output_filter(file[:-7]):
                    xml_file_path = os.path.join(root, file)
                    pic_file_path = os.path.join(root, util.file_name_no_ext(file) + ".png")
                    zipf.write(xml_file_path, os.path.relpath(xml_file_path, dest_temp_dir))
                    zipf.write(pic_file_path, os.path.relpath(pic_file_path, dest_temp_dir))
            util.flush_print('.')
    zipf.close()
    print("complete zip video files")


def detect_video_process(json_file_path, list_video_info, index, dest_temp_dir, final_vid_name):
    """
    多线程检测视频批量缩略图
    """
    global progress_bar

    video_info = list_video_info[index]

    dest_emo_dir = util.path_join(dest_temp_dir, video_info["emotion"])
    util.create_if_not_exist(dest_emo_dir)

    frame_as_sec = video_info.get('frame_as_sec', False)

    cap = cv2.VideoCapture(video_info["base_file"])
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frames = detect_video_desired_frames(video_info["start_frame"], video_info["end_frame"], frame_count, fps,
                                         frame_as_sec)

    frame_index = video_info["complete_frame"]
    frame_index = frame_index + 1 if frame_index > 0 else 0

    final_file_name = final_vid_name
    video_p_name_format = final_file_name + "_%02d.png"
    final_file_name += ".mp4"
    final_file_path = util.path_join(dest_emo_dir, final_file_name)

    if not path.isfile(final_file_path):
        video = VideoFileClip(video_info["base_file"])

        if frame_as_sec:
            video = video.subclip(frames[0], frames[len(frames) - 1])
        else:
            video = video.subclip(frames[0] / fps, frames[len(frames) - 1] / fps)

        video.write_videofile(final_file_path, threads=4, verbose=False, progress_bar=False)

    video_p_info_list = []
    while frame_index < len(frames):
        cap.set(cv2.CAP_PROP_POS_MSEC if frame_as_sec else cv2.CAP_PROP_POS_FRAMES,
                frames[frame_index] * 1000 if frame_as_sec else frames[frame_index])
        rs, frame = cap.read()

        if not rs:
            progress_bar.write("Read frame failed. frame:%s total:%s" % (frame_index, frame_count))
            break

        video_p_name = video_p_name_format % frame_index
        video_p_path = util.path_join(dest_emo_dir, video_p_name)

        # 保存截图
        util.cv2_image_to_file(frame, video_p_path)

        info = {"path": video_p_path, "index": frame_index}
        video_p_info_list.append(info)
        frame_index += 1

    start_time = time.time()
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        futures = {executor.submit(detect_pic_emotion_area, info["path"], width, height, video_info["emotion"]):
                       info for info in video_p_info_list}

        for future in concurrent.futures.as_completed(futures):
            info = futures[future]
            index = info["index"]
            try:
                future.result()
            except Exception as exc:
                mlogger.exception('Exception: row:%s' % info["path"])
                progress_bar.set_description("error process %s" % info["path"])
            else:
                video_info["complete_frame"] = max(video_info["complete_frame"], index)
                with open(json_file_path, "w", encoding='utf-8') as file:
                    json.dump(list_video_info, file, ensure_ascii=False, indent=4)

                progress_bar.set_description("%s of %s %0.2fp/s" %
                                             (index + 1, len(video_p_info_list),
                                              (index + 1) / (time.time() - start_time)))

    if video_info["complete_frame"] == len(frames) - 1:
        video_info["complete"] = True
        with open(json_file_path, "w", encoding='utf-8') as file:
            json.dump(list_video_info, file, ensure_ascii=False, indent=4)
    else:
        progress_bar.write("not complete all pic:complete:%s total:%s %s" % (
            video_info["complete_frame"], len(frames), final_file_name))


def detect_video_desired_frames(start_frame, end_frame, total_frame, fps, frame_as_sec):
    """
    计算视频截取的帧列表
    :param frame_as_sec: start&end frame实际含义为second
    :return:
    """

    if total_frame == 0:
        return []

    start_sec = start_frame
    end_sec = end_frame

    if frame_as_sec:
        start_frame = int(start_frame * fps)
        end_frame = int(end_frame * fps)

    if end_frame >= total_frame:
        end_frame = total_frame - 1
    if end_frame < 0:
        end_frame = 0
    if start_frame > end_frame:
        start_frame = end_frame
    if start_frame < 0:
        start_frame = 0

    gap_count = end_frame - start_frame + 1

    if gap_count <= 30:
        desired_count = gap_count
    elif gap_count >= 200:
        desired_count = 50
    else:
        desired_count = int(30 + math.log2(gap_count - 29))
    frames = []

    for i in range(0, desired_count):
        if frame_as_sec:
            index = start_sec + gap_count * i / desired_count / fps
        else:
            index = start_frame + int(gap_count * i / desired_count)
        frames.append(index)
    return frames


def detect_pic_emotion_area(pic_path, width, height, emotion, xml_pic_path=None):
    """
    检测表情区域并写入xml
    :param pic_path: 图片读取路径
    :param width: 
    :param height: 
    :param emotion: 
    :param xml_pic_path: 生成图片路径对应的xml信息
    :return: 
    """
    result_json = emotionapi.detect_file(pic_path)
    emotion_result = emotionapi.find_final_emotion(result_json)

    if emotion_result['type'] in (const.EMOTION_ZERO, const.EMOTION_ERROR):
        print("no face found:%s" % util.file_name(pic_path))
        return

    labelxml.create_label_file(
        pic_path if xml_pic_path is None else xml_pic_path, width, height, emotion,
        emotion_result['left'], emotion_result['top'],
        emotion_result['width'], emotion_result['height'])


def cmd_label_pic_list(pic_dir, dest_dir):
    """
    对已知图片进行标记
    """
    global progress_bar

    pic_dir_name = util.file_name(pic_dir)

    sad_dir = path.join(pic_dir, "sadness")
    anger_dir = path.join(pic_dir, "anger")

    pic_list = []

    def is_image_file(file):
        return file.lower().endswith("jpg") or file.lower().endswith("jpeg") or file.lower().endswith("png")

    def is_png_file(file):
        return file.lower().endswith("png")

    for pic_dir_info in [{"pic_dir": sad_dir, "type": "sadness"}, {"pic_dir": anger_dir, "type": "anger"}]:
        for root, dirs, files in os.walk(pic_dir_info["pic_dir"]):
            for file in files:
                xml = util.file_name_no_ext(file) + ".xml"
                if is_image_file(file):
                    if not path.exists(path.join(root, xml)):
                        with Image.open(path.join(root, file)) as img:
                            width, height = img.size
                            pic_list.append({"path": path.join(root, file),
                                             "type": pic_dir_info["type"],
                                             "width": width,
                                             "height": height})

    progress_bar = tqdm(total=(len(pic_list)), bar_format='{desc}{percentage:3.0f}%|{bar}|{elapsed}')

    def fix_to_png_path(origin_path):
        if util.file_name_no_ext(origin_path).lower().endswith("png"):
            return origin_path
        else:
            return path.join(util.file_folder(origin_path), util.file_name_no_ext(origin_path) + ".png")

    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            futures = {
                executor.submit(
                    detect_pic_emotion_area, info["path"], info["width"], info["height"], info["type"],
                    xml_pic_path=fix_to_png_path(info["path"])): info
                for info in pic_list}

            for future in concurrent.futures.as_completed(futures):
                info = futures[future]
                try:
                    future.result()
                except Exception as exc:
                    progress_bar.write("label failed:%s error:%s" % (info["path"], str(exc)))
                    progress_bar.update(1)
                else:
                    progress_bar.set_description("%s of %s" % (pic_list.index(info), len(pic_list)))
                    progress_bar.update(1)

    except KeyboardInterrupt:
        print("interrupt end")
        return

    print("\nStart zip files\n")
    zip_f = zipfile.ZipFile(path.join(dest_dir, pic_dir_name + ".final.zip"), 'w', zipfile.ZIP_DEFLATED)

    temp_dir = path.join(dest_dir, "temp_" + pic_dir_name)
    if not path.exists(temp_dir):
        os.makedirs(temp_dir)

    count_valid = 0
    count_unknown = 0
    count_small = 0
    for root, dirs, files in os.walk(pic_dir):
        for file in files:
            if is_image_file(file):
                util.flush_print(".")
                pic_path = path.join(root, file)
                pic_in_zip_path = path.relpath(pic_path, pic_dir)
                xml_path = path.join(root, util.file_name_no_ext(file) + ".xml")
                xml_in_zip_path = path.relpath(xml_path, pic_dir)
                if not is_png_file(file):
                    pic_in_zip_path = path.relpath(path.join(root, util.file_name_no_ext(file) + ".png"), pic_dir)
                    im = Image.open(pic_path)
                    pic_path = path.join(temp_dir, util.file_name_no_ext(file) + ".png")
                    im.convert('RGB').save(pic_path)
                if not path.exists(xml_path):
                    zip_f.write(pic_path, path.join("unknown", pic_in_zip_path))
                    count_unknown += 1
                    continue

                size = labelxml.read_face_size_from_xml(xml_path)

                if size is None:
                    zip_f.write(pic_path, path.join("unknown", pic_in_zip_path))
                    count_unknown += 1
                    continue
                if size["width"] < 200 or size["height"] < 200:
                    zip_f.write(pic_path, path.join("too_small", pic_in_zip_path))
                    zip_f.write(pic_path, path.join("too_small", xml_in_zip_path))
                    count_small += 1
                    continue

                zip_f.write(pic_path, pic_in_zip_path)
                zip_f.write(xml_path, xml_in_zip_path)
                count_valid += 1

    zip_f.close()
    shutil.rmtree(temp_dir)

    print("\nvalid:%s unknown:%s small:%s" % (count_valid, count_unknown, count_small))


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "h",
            ["help", "db=", "test", "cp_all_emo", "cp_pic_list", "cp_vid_list", "det_vid_list", "label_pic_list",
             "type=", "gt=", "out_prefix="])
    except getopt.GetoptError:
        usage()
        sys.exit()

    cmd = "--help"
    db_file = None
    emotion_type = None
    gt_value = 0.7
    out_prefix = "out"
    for o, v in opts:
        if o in ("-h", "--help"):
            cmd = "--help"
        elif o == "--db":
            db_file = v
        elif o in ("--test", "--cp_all_emo", "--cp_pic_list", "--cp_vid_list", "--det_vid_list", "--label_pic_list"):
            cmd = o
        elif o in "--type":
            emotion_type = v
        elif o in "--gt":
            gt_value = v
        elif o in "--out_prefix":
            out_prefix = v

    sqlite_helper = SQLiteHelper(db_file)

    logger.setup_logger("filetask.log", "filetask_%s.log" % util.get_mmddhhmm())
    mlogger = logger.get_logger("filetask.log")

    if cmd == "--help":
        usage()
        sys.exit()

    if cmd == "--test":
        if len(args) == 2:
            cmd_test(args[0], args[1])
        else:
            usage()
            sys.exit()

    if cmd == "--cp_all_emo":
        if len(args) == 1:
            cmd_copy_all_emotion(args[0], emotion_type, gt_value, out_prefix=out_prefix)
        else:
            usage()
            sys.exit()

    if cmd == "--cp_pic_list":
        if len(args) == 2:
            cmd_copy_pic_list(args[0], args[1], out_prefix=out_prefix)
        else:
            usage()
            sys.exit()

    if cmd == "--cp_vid_list":
        if len(args) >= 2:
            cmd_copy_vid_list(args[0], args[1], None if len(args) == 2 else args[2], out_prefix=out_prefix)
        else:
            usage()
            sys.exit()

    if cmd == "--det_vid_list":
        if len(args) == 2:
            cmd_detect_vid_list(args[0], args[1])
        else:
            usage()
            sys.exit()

    if cmd == "--label_pic_list":
        if len(args) == 2:
            cmd_label_pic_list(args[0], args[1])
        else:
            usage()
            sys.exit()

    print("Complete")
