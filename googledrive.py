import re

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

import util

google_drive = None


def usage():
    print("usage:")
    print("download <id> <file_path>")
    print("upload <file_path>")
    print("exit")


def auth():
    global google_drive
    gauth = GoogleAuth()
    gauth.CommandLineAuth()
    google_drive = GoogleDrive(gauth)


def download(id, to_path):
    print("downloading...")
    file = google_drive.CreateFile({'id': id})
    file.GetContentFile(to_path)
    print("download success")


def upload(src_path):
    print("uploading...")
    file = google_drive.CreateFile({'title': util.file_name(src_path)})
    file.SetContentFile(src_path)
    file.Upload()
    print("upload success")


if __name__ == "__main__":
    auth()

    print("Please input command:")

    regexs = [re.compile(r'^download (\S+) (\S+)$'), re.compile(r'^upload (\S+)$'), re.compile(r'^exit$'),
              re.compile(r'^help$')]
    funcs = {
        '^download (\S+) (\S+)$': download,
        '^upload (\S+)$': upload,
        '^exit$': exit,
        '^help$': usage,
    }

    while True:
        command = input()

        index = 0
        for regex in regexs:
            match = regex.match(command)
            if match:
                funcs[regex.pattern](*match.groups())
                break
            else:
                index += 1

        if index == len(regexs):
            print("wrong command")
