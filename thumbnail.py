#!/usr/bin/python3
# coding: utf-8

import getopt
import os
import sys
from os import path

import cv2
from tqdm import tqdm

import util
from face_detector import Detector
from sqlitehelper import SQLiteHelper

AFTER_START_SECOND = 3 * 60
BEFORE_END_SECOND = 3 * 60
STEP_FIND_FACE_SECOND = 2
STEP_NEXT_FACE_SECOND = 6

THUMBNAIL_SUB_DIR = "output"

face_detector = None
progress_bar = None
sqlite_helper = None


class VideoInfo:
    def __init__(self, file_dir, file_name):
        self.file_dir = file_dir
        self.file_name = file_name

    def file_path(self):
        return path.join(self.file_dir, self.file_name)


def usage():
    print("usage: thumbnail.py [-h] [--db] [-p] dirs...")
    print("-h   : print this help message and exit (also --help)")
    print("-p   : the base path of input dirs")
    print("--db : the db file name")


def is_video(file_name):
    lower = file_name.lower()
    return lower.endswith(".mkv") or lower.endswith(".mp4")


def process_dirs(dirs):
    result = []
    for file_dir in dirs:
        sub_files = os.listdir(file_dir)
        sub_files.sort()
        for file_name in sub_files:
            if path.isfile(path.join(file_dir, file_name)) and is_video(file_name):
                result.append(VideoInfo(file_dir, file_name))

    print()
    print("=" * 20 + "Video List" + "=" * 20)
    for video_info in result:
        print("    %s" % video_info.file_path())
    print("=" * 50)
    print()

    return result


def process_video_list(list):
    global progress_bar
    progress_bar = tqdm(total=(100 * len(list)), bar_format='{desc}{percentage:3.0f}%|{bar}|{elapsed}')
    try:
        for index, video in enumerate(list):
            process_video(video)
    except KeyboardInterrupt:
        pass
    progress_bar.close()


def process_video(video):
    global progress_bar
    progress_bar.write("Start process %s" % video.file_name)

    thumb_dir = path.join(video.file_dir, THUMBNAIL_SUB_DIR)
    if not path.exists(thumb_dir):
        os.mkdir(thumb_dir)

    # 截图文件名格式（移除x264\AAC等信息）
    thumb_p_name_format = util.pure_public_file_name(util.file_name_no_ext(video.file_name)) + "_%06d.png"

    cap = cv2.VideoCapture(video.file_path())
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # Start work after frame already record in db
    db_last_frame = query_last_frame(video)

    step_find_next = STEP_FIND_FACE_SECOND * fps
    step_next_face = STEP_NEXT_FACE_SECOND * fps

    frame_index = db_last_frame + step_next_face if db_last_frame > 0 else AFTER_START_SECOND * fps
    last_frame = frame_count - 1 - BEFORE_END_SECOND * fps

    add_progress_left = 0
    while frame_index < last_frame:

        # Update progress bar
        progress_bar.set_description("Frame %s of %s" % (frame_index, frame_count))
        add_progress = int(100 * add_progress_left / frame_count)
        add_progress_left -= int(add_progress * frame_count / 100)
        progress_bar.update(add_progress)

        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
        rs, frame = cap.read()

        if not rs:
            progress_bar.write("Read frame failed. frame:%s total:%s" % (frame_index, frame_count))
            break

        faces = face_detector.detect_faces(frame, True)

        if len(faces) > 0:
            frame_file_name = thumb_p_name_format % frame_index

            # cv2无法保存到中文路径名
            # cv2.imwrite(util.path_join(thumb_dir, frame_file_name), frame)

            # 保存截图
            util.cv2_image_to_file(frame, util.path_join(thumb_dir, frame_file_name))

            insert_record(video.file_dir, video.file_name, width, height, thumb_dir, frame_index, frame_file_name)
            frame_index += step_next_face
            add_progress_left += step_next_face
            continue
        frame_index += step_find_next
        add_progress_left += step_find_next


def query_last_frame(video):
    return sqlite_helper.query_last_frame(util.unified_dir(video.file_dir), video.file_name)


def insert_record(v_dir, v_name, v_width, v_height, out_dir, frame, tb_p_name):
    sqlite_helper.insert(video_dir=util.unified_dir(v_dir), video_name=v_name,
                         video_width=v_width, video_height=v_height, thumb_dir=util.unified_dir(out_dir),
                         frame_index=frame, thumb_p_name=tb_p_name)


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hp:", ["help", "path=", "db="])
    except getopt.GetoptError:
        usage()
        sys.exit()

    input_path = ""
    db_file = None

    for o, v in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o == "--db":
            db_file = v
        elif o in ("-p", "--path"):
            input_path = v

    valid_dir = []

    for a in args:
        abspath = path.abspath(a) if not input_path else path.join(input_path, a)
        if path.isdir(abspath) and path.exists(abspath):
            valid_dir.append(abspath)

    if len(valid_dir) == 0:
        print("There is no valid directories, please check your input")
        sys.exit()

    face_detector = Detector()
    sqlite_helper = SQLiteHelper(db_file)
    sqlite_helper.create_no_exist()

    video_list = process_dirs(valid_dir)
    process_video_list(video_list)
    print("Complete")
