import codecs
import os
import xml.etree.ElementTree

from yattag import Doc, indent

import util


def create_label_file(thumb_path, width, height, name, fleft, ftop, fwidth, fheight):
    file_no_ext = util.file_name_no_ext(thumb_path)
    file_name = util.file_name(thumb_path)

    file_path = os.path.join('download', file_name)

    xml_file_name = file_no_ext + ".xml"
    xml_file_path = os.path.join(os.path.dirname(thumb_path), xml_file_name)

    xml = label_xml('download', file_no_ext, file_path,
                    width, height, name, fleft, ftop, fleft + fwidth - 1, ftop + fheight - 1)
    with codecs.open(xml_file_path, 'w', encoding='utf8') as f:
        f.write(xml)


def label_xml(folder, file_name, file_path, width, height, name, xmin, ymin, xmax, ymax):
    doc, tag, text, line = Doc().ttl()

    with tag('annotation'):
        line('folder', folder)
        line('filename', file_name)
        line('path', file_path)

        with tag('source'):
            line('database', 'Unknown')

        with tag('size'):
            line('width', width)
            line('height', height)
            line('depth', 3)

        line('segmented', 0)

        with tag('object'):
            line('name', name)
            line('pose', 'Unspecified')
            line('truncated', 0)
            line('difficult', 0)
            with tag('bndbox'):
                line('xmin', xmin)
                line('ymin', ymin)
                line('xmax', xmax)
                line('ymax', ymax)

    return indent(doc.getvalue())


def read_face_size_from_xml(xml_file):
    root = xml.etree.ElementTree.parse(xml_file).getroot()
    try:
        size = root.find("object").find("bndbox")
        xmin = int(size.find("xmin").text)
        xmax = int(size.find("xmax").text)
        ymin = int(size.find("ymin").text)
        ymax = int(size.find("ymax").text)
        return {"width": xmax - xmin, "height": ymax - ymin}
    except Exception as ex:
        print("read xml exception:" + str(ex))
        return None


if __name__ == "__main__":
    print(read_face_size_from_xml("test.xml"))
