from moviepy.editor import *
import sys


def split(src_file, output_file, start_frame, end_frame):
    video = VideoFileClip(src_file)
    video = video.subclip(start_frame / video.fps, end_frame / video.fps)
    video.write_videofile(output_file, threads=4, verbose=True, progress_bar=False)

if __name__ == "__main__":
    split(sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]))
