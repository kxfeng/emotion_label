import getopt
import sqlite3
import sys

DEFAULT_DB_FILE = "record.db"
SQL_CREATE_TBL = '''CREATE TABLE IF NOT EXISTS record (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    video_dir TEXT, video_name TEXT,
    thumb_dir TEXT, thumb_p_name TEXT, thumb_v_name TEXT, thumb_xml TEXT,
    video_width INTEGER, video_height INTEGER,
    frame_index INTEGER, emotion_type TEXT, emotion_value REAL,
    face_left INTEGER, face_top INTEGER, face_width INTEGER, face_height INTEGER,
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
)'''

TABLE_NAME = "record"

ID = "id"
VIDEO_DIR = "video_dir"
VIDEO_NAME = "video_name"
THUMB_DIR = "thumb_dir"
THUMB_P_NAME = "thumb_p_name"
THUMB_V_NAME = "thumb_v_name"
THUMB_XML = "thumb_xml"
VIDEO_WIDTH = "video_width"
VIDEO_HEIGHT = "video_height"
FRAME_INDEX = "frame_index"
EMOTION_TYPE = "emotion_type"
EMOTION_VALUE = "emotion_value"
FACE_LEFT = "face_left"
FACE_TOP = "face_top"
FACE_WIDTH = "face_width"
FACE_HEIGHT = "face_height"
UPDATE_TIME = "update_time"


def usage():
    print("usage: sqlitehelper.py [-d]")
    print("-h   : print this help message and exit (also --help)")
    print("-d   : drop and create a new table")


def sql_row_dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


# class Singleton(object):
#     _instance = None
#
#     def __new__(cls, *args):
#         if not cls._instance:
#             cls._instance = super(Singleton, cls).__new__(cls, *args)
#         return cls._instance


class SQLiteHelper:
    def __init__(self, db_file):
        self.connection = sqlite3.connect(DEFAULT_DB_FILE if db_file is None else db_file, timeout=10)
        self.connection.row_factory = sql_row_dict_factory

    def create_no_exist(self):
        self.connection.execute(SQL_CREATE_TBL)
        self.connection.commit()

    def drop_create(self):
        self.connection.execute("DROP TABLE record")
        self.connection.execute(SQL_CREATE_TBL)
        self.connection.commit()

    def close(self):
        self.connection.close()

    def insert(self, video_dir="", video_name="", thumb_dir="", thumb_p_name="", thumb_v_name="",
               video_width=0, video_height=0, frame_index=0):
        values = "('%s', '%s', '%s', '%s', '%s', %s, %s, %s)" % \
                 (video_dir, video_name, thumb_dir, thumb_p_name, thumb_v_name, video_width, video_height, frame_index)

        self.connection.execute("INSERT INTO record \
            (video_dir, video_name, thumb_dir, thumb_p_name, thumb_v_name, video_width, video_height, frame_index) \
            VALUES " + values)
        self.connection.commit()

    def update(self, id, thumb_xml=None, thumb_v_name=None, emotion_type=None, emotion_value=None,
               face_left=None, face_top=None, face_width=None, face_height=None):

        set_values = []
        if thumb_xml is not None:
            set_values.append("thumb_xml = '%s'" % thumb_xml)

        if thumb_v_name is not None:
            set_values.append("thumb_v_name = '%s'" % thumb_v_name)

        if emotion_type is not None:
            set_values.append("emotion_type = '%s'" % emotion_type)

        if emotion_value is not None:
            set_values.append("emotion_value = %s" % emotion_value)

        if face_left is not None:
            set_values.append("face_left = %s" % face_left)

        if face_top is not None:
            set_values.append("face_top = %s" % face_top)

        if face_width is not None:
            set_values.append("face_width = %s" % face_width)

        if face_height is not None:
            set_values.append("face_height = %s" % face_height)

        sql = "UPDATE record SET " + ", ".join(set_values) + " WHERE id = " + str(id)
        self.connection.execute(sql)
        self.connection.commit()

    def query(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return cursor

    # types: emotion types in array
    # gt: greater than
    # min_size: emotion min size
    def query_emotion(self, types, gt=None, min_size=None, limit=None):
        cursor = self.connection.cursor()

        where_types = ""
        in_types = types[:]
        if None in types:
            in_types.remove(None)
            where_types = " emotion_type IS NULL OR "
        in_sql = "(" + ",".join(["'%s'"] * len(in_types)) + ")"
        in_sql %= tuple(in_types)
        where_types += " emotion_type IN " + in_sql

        where_gt = ""
        if gt is not None:
            where_gt = " AND emotion_value >= %s " % gt

        where_size = ""
        if min_size is not None:
            where_size = " AND face_width >= %s AND face_height >= %s " % (min_size, min_size)

        limit_sql = ""
        if limit is not None:
            limit_sql = " LIMIT %s" % limit

        sql = "SELECT * FROM record WHERE (%s) %s %s %s" % (where_types, where_gt, where_size, limit_sql)
        cursor.execute(sql)
        return cursor

    def query_last_frame(self, video_dir, video_name):
        cursor = self.connection.cursor()

        cursor.execute("SELECT frame_index FROM record WHERE video_dir=? AND video_name=? ORDER BY frame_index DESC",
                       (video_dir, video_name))
        item = cursor.fetchone()

        if item is None:
            return 0

        return item["frame_index"]


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "d", ["drop", "db="])
    except getopt.GetoptError:
        usage()
        sys.exit()

    db_file = None
    drop = False
    for o, v in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o == "--db":
            db_file = v
        elif o in ("-d", "--drop"):
            drop = True

    sqlite_helper = SQLiteHelper(db_file)

    if drop:
        sqlite_helper.drop_create()
    else:
        sqlite_helper.create_no_exist()

    sqlite_helper.insert(video_dir="D:\Torrent", video_name="Umimachi.Diary.2015.BluRay.720p.x264.DTS-CMCT.mkv",
                         thumb_dir="D:\Torrent\output", thumb_p_name="123.jpg", thumb_v_name="123.mp4",
                         video_width=720, video_height=1080, frame_index=100)
    cursor = sqlite_helper.query_emotion([None])
    for row in cursor:
        print("id=%s, thumb_p_name=%s, emotion_type=%s" % (row['id'], row['thumb_p_name'], row['emotion_type']))

    print("query_last_frame:",
          sqlite_helper.query_last_frame("D:\Torrent", "Umimachi.Diary.2015.BluRay.720p.x264.DTS-CMCT.mkv"))
