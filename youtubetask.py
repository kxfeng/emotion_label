#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import getopt
import json
import os
import sys

import youtube_dl
from youtube_dl.utils import YoutubeDLError

import filetask
import util
from util import flush_print


def usage():
    print("usage:")
    print("--help   : print this help message and exit (also -h)")
    print("--dl <json_list> <dl_path>")
    print("--dt <json_list> <dest_dir>")


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'downloading':
        flush_print(".")

    if d['status'] == 'finished':
        flush_print("\n")
        print('Converting ...')

    if d['status'] == 'error':
        flush_print("\n")
        print('Download error')


ydl_opts = {
    'format': 'bestvideo[height<1080]+bestaudio/best',
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
    'merge_output_format': 'mkv'
}


def download_task(json_file, dl_path):
    util.create_if_not_exist(dl_path)

    with open(json_file) as file:
        list_video_info = json.load(file)

    try:
        download_urls(json_file, dl_path, list_video_info)

        new_json = util.path_join(util.file_folder(json_file), util.file_name_no_ext(json_file) + "_final.json")

        transform_json_to_detect(json_file, new_json)
    except KeyboardInterrupt:
        print("interrupt end")
        return


def download_urls(json_file, dl_path, list_video_info):
    clear_download_cache(dl_path)

    ydl_opts['outtmpl'] = util.path_join(dl_path, '%(id)s.%(resolution)s.%(ext)s')

    for video_info in list_video_info:

        if (video_info.get("file", None) is not None) and os.path.exists(video_info["file"]):
            # support user copied file
            continue

        final_file = None
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:

            video_id = None
            try:
                dl_url = video_info['url']
                info_dict = ydl.extract_info(dl_url, download=False)
                video_id = info_dict.get("id", None)
                video_title = info_dict.get('title', None)
                print("id=%s title=%s" % (video_id, video_title))

                final_file = ydl.prepare_filename(info_dict)

                def save_video_file():
                    video_info["file"] = final_file
                    with open(json_file, "w", encoding='utf-8') as file:
                        json.dump(list_video_info, file, ensure_ascii=False, indent=4, sort_keys=True)

                if os.path.exists(final_file):
                    save_video_file()
                    continue
                ydl.download([dl_url])
                save_video_file()
            except YoutubeDLError as ex:
                print("download id=%s error:%s" % (video_id, ex))
                if (final_file is not None) and os.path.exists(final_file):
                    os.remove(final_file)
                clear_download_cache(dl_path)


def transform_json_to_detect(original_json, transformed_json):
    with open(original_json) as file:
        list_original_video_info = json.load(file)

    list_new_transformed_json = []
    list_old_transformed_json = []

    if os.path.exists(transformed_json):
        with open(transformed_json) as file:
            list_old_transformed_json = json.load(file)

    def find_math_item_from_json(base_file, start_frame, end_frame):
        for item in list_old_transformed_json:
            if item['base_file'] == base_file and item['start_frame'] == start_frame and item['end_frame'] == end_frame:
                return item
        return None

    failed_index = []
    for index, video_info in enumerate(list_original_video_info):
        # ignore item which failed downloading
        if not video_info.get("file"):
            failed_index.append(index)
            continue

        base_file = video_info['file']
        start_second = util.hhmmss_to_second(video_info['start'])
        end_second = util.hhmmss_to_second(video_info['end'])

        # if transformed item exists, not override it
        transformed = find_math_item_from_json(base_file, start_second, end_second)

        if not transformed:
            transformed = {
                'complete_frame': 0,
                'emotion': video_info['emotion'],
                'complete': False,
                'start_frame': start_second,
                'end_frame': end_second,
                'base_file': base_file,
                'frame_as_sec': True
            }
        list_new_transformed_json.append(transformed)
        with open(transformed_json, "w", encoding='utf-8') as file:
            json.dump(list_new_transformed_json, file, ensure_ascii=False, indent=4)

    if len(failed_index) > 0:
        print("failed count:%s index:%s" % (len(failed_index), failed_index))


def detect_task(json_file, out_path):
    def final_vid_name_creator(base_file, start_frame):
        return "%s_%s" % (util.file_name_no_ext(base_file), start_frame)

    final_zip_name = "%s.zip" % util.file_name_no_ext(json_file)

    filetask.common_detect_vid_list(json_file, out_path, final_vid_name_creator, final_zip_name)


def clear_download_cache(dl_path):
    cache_ext_list = ['part', 'ytdl', 'mp4a', 'webm']
    files = [os.path.join(dl_path, f) for f in os.listdir(dl_path) if os.path.isfile(os.path.join(dl_path, f))]
    for f in files:
        if util.file_ext(f) in cache_ext_list:
            os.remove(f)


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "h", ["help", "dl", "dt"])
    except getopt.GetoptError:
        usage()
        sys.exit()

    for o, v in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()

        if o == "--dl":
            if len(args) == 2:
                download_task(args[0], args[1])
            else:
                usage()
                sys.exit()

        if o == "--dt":
            if len(args) == 2:
                detect_task(args[0], args[1])
            else:
                usage()
                sys.exit()

    print("Complete")
