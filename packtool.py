import getopt
import sys
import zipfile
from os import path

import infotask


def usage():
    print("usage:\npacktool.py [--split] [--part] [--dir] [--out_format] [zip_list]")
    print("options:")
    print("--split sadness|anger")
    print("--part int, bigger than 0")
    print("--dir optional, work dir")
    print("--out_format part zip file name format, eg: out_part{}.zip")


def split_part(count_list, part_len):
    """
    :param count_list:
    :param part_len:
    :return:
    """
    total_count = 0
    for arr in count_list:
        total_count += len(arr)

    aver_part_emo_count = int(total_count / part_len)

    part_file_count_list = [0] * part_len
    part_index = 0
    part_emo_count = 0
    for arr in count_list:
        inner_index = 0
        while inner_index < len(arr):
            item_len = 1 + arr[inner_index]["pic"] + arr[inner_index]["xml"]
            part_file_count_list[part_index] = part_file_count_list[part_index] + item_len
            part_emo_count += 1
            inner_index += 1

            if part_index < part_len - 1 and part_emo_count == aver_part_emo_count:
                part_index += 1
                part_emo_count = 0

    start = 0
    for i in range(part_len):
        part_file_count_list[i] = start + part_file_count_list[i]
        start = part_file_count_list[i]
    return part_file_count_list


def split_to_zip(split_type, part_len, work_dir, out_format, zip_list):
    # [["xxx.mp4", "xxx.png"],[]]
    zip_inner_file_list = []
    # [[{"vid":"xxx.mp4","pic": 16,"xml": 16}],[]]
    zip_inner_count_list = []

    for zip_path in zip_list:
        zip_file_info = infotask.get_zip_file_info(path.join(work_dir, zip_path))
        if zip_file_info.get(split_type) is None:
            zip_inner_file_list.append([])
            zip_inner_count_list.append([])
            continue
        zip_count_info = infotask.calc_pic_count_info(zip_file_info)

        zip_inner_file_list.append(zip_file_info[split_type])
        zip_inner_count_list.append(zip_count_info[split_type])

    part_list = split_part(zip_inner_count_list, part_len)

    part_index = 0
    write_count = 0
    zip_out = zipfile.ZipFile(path.join(work_dir, out_format.format(part_index + 1)), "w", zipfile.ZIP_DEFLATED)
    for zip_index, zip_path in enumerate(zip_list):
        original_zip = zipfile.ZipFile(path.join(work_dir, zip_path), 'r')

        for f in zip_inner_file_list[zip_index]:
            bs = original_zip.read("%s/%s" % (split_type, f))
            zip_out.writestr(f, bs)

            write_count += 1

            if write_count == part_list[part_index]:
                zip_out.close()
                print("complete write part %s" % (part_index + 1))

                part_index += 1

                if part_index < part_len:
                    zip_out = zipfile.ZipFile(path.join(work_dir, out_format.format(part_index + 1)), "w",
                                              zipfile.ZIP_DEFLATED)
                else:
                    break
        original_zip.close()
        if part_index == part_len:
            break


if __name__ == "__main__":
    split_type = None
    part_len = None
    work_dir = path.dirname(path.realpath(__file__))
    out_format = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help", "split=", "part=", "dir=", "out_format="])
    except getopt.GetoptError:
        usage()
        sys.exit()

    for o, v in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        if o == "--split":
            split_type = v
        if o == "--part":
            try:
                part_len = int(v)
            except ValueError:
                print("part_len should be a number")
                sys.exit()
        if o == "--dir":
            work_dir = v
        if o == "--out_format":
            out_format = v

    if split_type is None:
        print("split type should be set")
        sys.exit()

    if part_len is None or part_len < 1:
        print("part should be bigger than 0")
        sys.exit()

    if out_format is None:
        print("out_format should be defined")
        sys.exit()

    if len(args) < 1:
        usage()
        sys.exit()

    zip_list = args

    split_to_zip(split_type, part_len, work_dir, out_format, zip_list)
    print("success")
