import datetime
import ntpath
import os
import re
import shutil
import sys
import time

import cv2
from PIL import Image
from xpinyin import Pinyin


def flush_print(char):
    sys.stdout.write(char)
    sys.stdout.flush()


def unified_dir(dir):
    if dir is None:
        return dir
    if str.endswith(dir, os.path.sep):
        return dir
    dir = dir + os.path.sep
    return dir


def pure_public_file_name(name):
    if name is None:
        return name
    remove = re.sub("\.WEB-DL|\.1080p|\.720p|\.HDTV|\.BluRay|\.4K"
                    + "|\.AAC2\.0|\.AAC|\.AC3|\.2Audios|\.2Audio"
                    + "|\.H264|\.x264"
                    + "|\.Complete|-HDCTV|-HDWTV|-PureTV|-CMCTV|-CMCT|-npuer|-NGB"
                    + "|\(国粤双语\)"
                    , '', name, flags=re.I)
    to_py = Pinyin().get_pinyin(remove, "", convert="capitalize")
    return to_py


def create_if_not_exist(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)


def file_name_no_ext(path):
    return os.path.splitext(file_name(path))[0]


def file_folder(path):
    return os.path.dirname(path)


def file_name(path):
    return ntpath.basename(os.path.normpath(path))


def file_ext(path):
    return os.path.splitext(path)[1]


def path_join(path, subpath):
    return os.path.join(path, subpath)


def get_mmddhhmm():
    return datetime.datetime.today().strftime('%m%d%H%M')


def cv2_image_to_file(frame, file_path):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(frame)
    image.save(file_path)


def copy_to_dir(src_file, dest_dir):
    if not os.path.isfile(src_file):
        return

    src_file_name = file_name(src_file)
    dest_file = path_join(dest_dir, src_file_name)

    if not os.path.isfile(dest_file):
        shutil.copy(src_file, dest_file)


def copy_to_file(src_file, dest_file):
    if not os.path.isfile(src_file):
        return
    if not os.path.isfile(dest_file):
        shutil.copy(src_file, dest_file)


def convert_image_format(src_file, to_file):
    im = Image.open(src_file)
    im.save(to_file)


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def sleep_for_20_limit_minute(spent):
    if spent >= 3:
        return
    time.sleep(3.2 - spent)


def hhmmss_to_second(hhmmmss):
    arr = list(reversed(hhmmmss.split(":")))
    sum = 0
    base = 1
    for i in range(0, len(arr)):
        sum += int(arr[i]) * base
        base = base * 60
    return int(sum)
