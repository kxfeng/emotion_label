import concurrent.futures
import sys
import time
from os import path

import emotionapi
from sqlitehelper import SQLiteHelper


def detect_and_update_db(db, index, row):
    thumb_path = path.join(row['thumb_dir'], row['thumb_p_name'])
    result_json = emotionapi.detect_file(thumb_path)
    print(result_json)
    emotion_result = emotionapi.find_final_emotion(result_json)
    print(emotion_result)
    return index


if __name__ == "__main__":
    db = sys.argv[1]
    sqlite = SQLiteHelper(db)
    cursor = sqlite.query_emotion([None])
    list_rows = cursor.fetchall()
    list_len = len(list_rows)

    print("Start detect total:%s" % list_len)

    start_time = time.time()

    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        futures = [executor.submit(detect_and_update_db, db, index, row) for index, row in enumerate(list_rows)]
        for future in concurrent.futures.as_completed(futures):
            try:
                data = future.result()
            except Exception as exc:
                print('Exception %s' % exc)
            else:
                print('R:%s' % data)

    print("Complete:%ss" % (time.time() - start_time))
