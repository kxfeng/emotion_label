#!/usr/bin/python3
# coding: utf-8

import concurrent.futures
import getopt
import sys
import time
from os import path

from tqdm import tqdm

import const
import emotionapi
import logger
import sqlitehelper
import util
from sqlitehelper import SQLiteHelper

progress_bar = None
mlogger = None


def usage():
    print("usage: emotiontask.py [-h] [-t] [--db] [--thread]")
    print("-h   : print this help message and exit (also --help)")
    print("-t   : the emotion_type to detect: 1, none;2, none_error")
    print("--db : the db file name")
    print("--thread : the max thread count")


def detect_emotion(db, types, thread=None):
    global progress_bar

    sqlite_helper = SQLiteHelper(db)
    cursor = sqlite_helper.query_emotion(types)
    list_rows = cursor.fetchall()
    list_len = len(list_rows)

    print("Start detect total:%s" % list_len)

    progress_bar = tqdm(total=list_len, bar_format='{desc}{percentage:3.0f}%|{bar}|{elapsed}')

    start_time = time.time()

    if thread is None:
        thread = 8
    else:
        thread = int(thread)

    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=thread) as executor:
            futures_index = {executor.submit(detect_single_emotion, db, row): index for index, row in
                             enumerate(list_rows)}
            for future in concurrent.futures.as_completed(futures_index):
                index = futures_index[future]
                row = list_rows[index]
                try:
                    result = future.result()
                except Exception as exc:
                    mlogger.exception('Exception: row:%s' % row[sqlitehelper.THUMB_P_NAME])
                    progress_bar.update(1)
                else:
                    # mlogger.info('Result: row:%s result:%s' % (row[sqlitehelper.THUMB_P_NAME], result))
                    progress_bar.set_description("ID %s of %s %0.2fp/s" %
                                                 (row[sqlitehelper.ID], list_len,
                                                  (index + 1) / (time.time() - start_time)))
                    progress_bar.update(1)

    except KeyboardInterrupt:
        pass
    progress_bar.close()


def detect_single_emotion(db, row):
    thumb_path = path.join(row['thumb_dir'], row['thumb_p_name'])
    result_json = emotionapi.detect_file(thumb_path)
    emotion_result = emotionapi.find_final_emotion(result_json)

    thread_sqlite = SQLiteHelper(db)

    if emotion_result['type'] in (const.EMOTION_ZERO, const.EMOTION_ERROR):
        thread_sqlite.update(row['id'], emotion_type=emotion_result['type'])
        thread_sqlite.close()
        return emotion_result

    thread_sqlite.update(row['id'],
                         emotion_type=emotion_result['type'], emotion_value=emotion_result['value'],
                         face_left=emotion_result['left'], face_top=emotion_result['top'],
                         face_width=emotion_result['width'], face_height=emotion_result['height'])
    thread_sqlite.close()
    return emotion_result


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:", ["help", "type=", "db=", "thread="])
    except getopt.GetoptError:
        usage()
        sys.exit()

    detect_type = "none"
    db_file = None
    thread = None

    for o, v in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o == "--db":
            db_file = v
        elif o == "--thread":
            thread = v
        elif o in ("-t", "--type"):
            if v not in ('none', 'none_error'):
                usage()
                sys.exit()
            else:
                detect_type = v

    logger.setup_logger("emotiontask.log", "emotiontask_%s.log" % util.get_mmddhhmm())
    mlogger = logger.get_logger("emotiontask.log")

    if detect_type == 'none':
        detect_emotion(db_file, [None], thread)
    else:
        detect_emotion(db_file, [None, 'error'], thread)

    print("Complete")
